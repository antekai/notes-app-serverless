export default {
  MAX_ATTACHMENT_SIZE: 5000000,
  s3: {
    BUCKET: "notes-app-serverless" //YOUR_S3_UPLOADS_BUCKET_NAME
  },
  apiGateway: {
    URL: "https://8qyv1g2my5.execute-api.us-east-2.amazonaws.com/prod", //YOUR_API_GATEWAY_URL
    REGION: "us-east-2" //YOUR_API_GATEWAY_REGION
  },
    cognito: {
      USER_POOL_ID: "us-east-2_B6As0ge5I",
      APP_CLIENT_ID: "7oncnt3v3lbe2javtjn6l80444",
      REGION: "us-east-2",
      IDENTITY_POOL_ID: "us-east-2:7ff9f9b0-d1ec-44b8-a8f7-8852c7f77b22"
    }
  };